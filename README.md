# Darc City

<img src="https://gitlab.com/perelax/src_imgs/-/raw/main/Darc%20City/darc-city-sq.png"  width="120" height="120">
---

## Description
Theme based on City Lights and Darcula.  Darcula has been modified to have reduced coloring and a crisp background.

<img src="https://gitlab.com/perelax/src_imgs/-/raw/main/Darc%20City/darc-city-theme-screen.png" height="480" width="800">

## Notes
* To complete the theme, please add the following two lines to settings.json.

```
"window.titleBarStyle": "custom",
"editor.bracketPairColorization.enabled": false

```

* To install, copy Darc City into the `<user home>/.vscode/extensions` folder and restart Code.


## Authors
Perelax

## License
See LICENSE.

Feel like saying thank you?  

[![Buy Me A Coffee](https://cdn.buymeacoffee.com/buttons/default-orange.png){width=120}](https://www.buymeacoffee.com/perelax)



